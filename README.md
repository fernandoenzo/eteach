# eTeach

Virtual learning platform developed by Sindrosoft.

## Commit style

Commit messages must be written in english.

## Commit organization

We will use Git-Flow as the workflow working with git. The following link has all that needs to be known to use it:

[Git-Flow](http://nvie.com/posts/a-successful-git-branching-model/)

It's recommended to get accustomed to use it as soon as posible.

## Version naming

We will use the most known version naming system: x.y.z where:

    - x: MAJOR VERSION
    - y: MINOR VERSION
    - z: BUGFIXES or REVISION
