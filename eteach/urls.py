#encoding:utf-8

from django.conf.urls import patterns, include, url

from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^courses/', include('courses.urls')),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^accounts/', include('users.urls')),
    url(r'^$', 'users.views.login_eteach', name='index'),

)