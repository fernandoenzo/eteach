#encoding:utf-8

from django.contrib import admin
from courses.models import Course, Degree, StudentRegistered, LecturerRegistered, VirtualLecturerRegistered

class StudentRegisteredInline(admin.StackedInline):
    model = StudentRegistered
    extra = 1


class LecturerRegisteredInline(admin.StackedInline):
    model = LecturerRegistered
    extra = 1


class VirtualLecturerRegisteredInline(admin.StackedInline):
    model = VirtualLecturerRegistered
    extra = 1


class CourseAdmin(admin.ModelAdmin):
    inlines = (
        StudentRegisteredInline,
        LecturerRegisteredInline,
        VirtualLecturerRegisteredInline,
    )

admin.site.register(Course, CourseAdmin)
admin.site.register(Degree)
