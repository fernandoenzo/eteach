/**
 * Main screen functions file
 *
 * Contains all the code needed for the animations in
 * the main screen template
 *
 * Author: Lorenzo Ruiz
 * Version: 1.0
 *
 */
var MainHeader = function(subHeader, userMenu) {

	function init() {
		
	}

	function slideSubHeader() {
		// First hide the user menu if it's slided
		userMenu.slideUp(function() {
			subHeader.slideToggle();
		});  
	}

	function slideUserMenu() {
		// First hide the subheader if it's slided
		subHeader.slideUp(function() {
			userMenu.slideToggle();
		});
	}

	return {
		init:init(),
		slideSubHeader:slideSubHeader,
		slideUserMenu:slideUserMenu
	}

};


var Course = function() {

	function showOrHideEvents(e) {
		var clickedCourse = $(e.target);

		if(clickedCourse.hasClass('course-header-hidden')) {
			clickedCourse.siblings('.course-events').fadeIn(function() {
				clickedCourse.removeClass('course-header-hidden').addClass('course-header-shown');
			});
		}
		else if(clickedCourse.hasClass('course-header-shown')) {
			clickedCourse.siblings('.course-events').fadeOut(function() {
				clickedCourse.removeClass('course-header-shown').addClass('course-header-hidden');
			});
		}
	}

	return {
		showOrHideEvents:showOrHideEvents
	}

}


$(document).ready(function() {

	var mh = new MainHeader($('.sub-header'), $('#user-menu'));
	var c = new Course();
	
	// Assign click events to header main menu
	$('#subheader').click(mh.slideSubHeader);
	$('#user').click(mh.slideUserMenu);

	// Assign click events to courses
	$('.course-header').click(c.showOrHideEvents);

	// Register courses block
	var homeCourses = new Block('home-courses');
});