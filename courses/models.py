#encoding:utf-8
from django.core.validators import RegexValidator

from django.db import models
from users.models import Actor


class Course(models.Model):
    name = models.CharField(max_length=50)
    syllabus = models.FileField(upload_to='courses', blank=True)  # This is provisional
    academic_year = models.CharField(max_length=9, validators=[RegexValidator('^\d{4}/\d{4}$')], blank=True)  # 2013/2014
    degree = models.ForeignKey('Degree', blank=True, null=True)
    students = models.ManyToManyField(Actor, through='StudentRegistered', related_name='students')
    lecturers = models.ManyToManyField(Actor, through='LecturerRegistered', related_name='lecturers')
    coordinator = models.ForeignKey(Actor, blank=True, null=True)
    virtual_lecturer = models.ManyToManyField(Actor, through='VirtualLecturerRegistered', related_name='virtual_lecturer')

    def __str__(self):
        return self.name + ' ' + self.academic_year


class Degree(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


# Association class definitions

class StudentRegistered(models.Model):
    hide = models.BooleanField(default=False)
    student = models.ForeignKey(Actor)
    course = models.ForeignKey(Course)

    def __str__(self):
        return self.student.first_name + ' - ' + self.course.name


class LecturerRegistered(models.Model):
    hide = models.BooleanField(default=False)
    lecturer = models.ForeignKey(Actor)
    course = models.ForeignKey(Course)

    def __str__(self):
        return self.lecturer.first_name + ' - ' + self.course.name


class VirtualLecturerRegistered(models.Model):
    hide = models.BooleanField(default=False)
    virtual_lecturer = models.ForeignKey(Actor)
    course = models.ForeignKey(Course)

    def __str__(self):
        return self.virtual_lecturer.first_name + ' - ' + self.course.name