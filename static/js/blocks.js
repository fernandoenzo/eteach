/**
 * UI blocks functions file
 *
 * Contains all the code used in the ui blocks of the
 * applications like settings, show dialogs, etc...
 *
 * Author: Lorenzo Ruiz
 * Version: 1.0
 *
 */

/**
 * Models a block of the ui and sets all the events that
 * it can responds to.
 *
 * Structure:
 * 
 *    Block (.ui-block)
 *        Header (.ui-block-header) 
 *        SettingsIcon (.ui-block-settings-icon)
 *        Wrapper (.ui-block-wrapper)
 *            Content (.ui-block-content)
 *            Settings (.ui-block-settings)
 */
var Block = function(blockId) {

	if( ! (this instanceof Block) ) {
    	return new Block(blockId);
    }

	var block = null;
	var blockHeader = null;
	var blockContent = null;
	var blockSettings = null;
    var blockWrapper = null;
	var blockSettingsIcon = null;
	var settingsShown = false;

	function init() {
		if(blockId.substring(0, 1) == '#') {
			block = $(blockId);
		}
		else {
			block = $('#' + blockId);
		}
		blockHeader = block.children('.ui-block-header');
		blockSettingsIcon = blockHeader.children('.ui-block-settings-icon');
		blockSettingsIcon = blockSettingsIcon.children('img')
		blockWrapper = block.children('.ui-block-wrapper');
		blockContent = blockWrapper.children('.ui-block-content');
		blockSettings = blockWrapper.children('.ui-block-settings');

		__attachEvents();
	}

	function __attachEvents() {
		blockSettingsIcon.click(__toggleSettings);
	}

	function __toggleSettings() {
		__rotateBlockSettingsIcon();
		if(settingsShown) {
			settingsShown = false;
			__showContent();
		}
		else {
			settingsShown = true;
			__hideContent();
		}
	}

	function __rotateBlockSettingsIcon() {
		$({deg: 0}).animate({deg: 90}, {
			duration: 1000,
			step: function(now) {
				blockSettingsIcon.css({
					transform: 'rotate(' + now + 'deg)'
				});
			}
		});
	}

	function __showContent() {
		blockSettings.slideUp(function() {
			blockContent.slideDown();
		});
	}

	function __hideContent() {
		blockContent.slideUp(function() {
			blockSettings.slideDown();
		});
	}

	return {
		init: init()
	}

};


/**
 * Models a tabbed content inside a block
 *
 * Structure:
 *
 *    Tabbed content (.tabbed-content)
 *        Tabs (.tabs li)
 *          Active tab (.active-tab)
 *        Tabs content (.tabs-content)
 *          Selected tab content (.active-content)
 */
var Tabs = function() {

    var tabbedContent = null;
    var tabs = null;
    var selectedTab = null;
    var tabsContent = null;
    var tabContents = null;
    var activeContent = null;

    function init() {
        tabbedContent = $('.tabbed-content');
        tabs = tabbedContent.find('.tabs li');
        selectedTab = tabbedContent.find('.active-tab');
        tabsContent = tabbedContent.find('.tabs-content');
        tabContents = tabsContent.children('div');
        activeContent = tabsContent.children('.active-content');

        __attachEvents();
    }

    function __attachEvents() {
        tabs.click(__makeActive);
    }

    function __makeActive(e) {
        var clickedTab = $(e.target);

        if(clickedTab.hasClass('active-tab')) {
            return;  // It's already the active tab
        }

        var clickedTabId = clickedTab.attr('class');
        var clickedTabContent = tabContents.siblings('.' + clickedTabId + '-content');

        tabs.removeClass('active-tab');
        clickedTab.addClass('active-tab');

        tabContents.removeClass('active-content');
        clickedTabContent.addClass('active-content');
    }

    return {
        init: init()
    }

}();  // This function autoruns itself