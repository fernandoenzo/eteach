/**
 * Utils functions file
 *
 * Contains code of utility functions
 * and used in the whole application
 *
 * Author: Lorenzo Ruiz
 * Version: 1.0
 *
 */
var Utils = function() {

    function init() {

    }

    function setupAjaxForCSRF() {
        var csrftoken = $.cookie('csrftoken');

        $.ajaxSetup({
            crossDomain: false,
            beforeSend: function(xhr, settings) {
                if( ! __csrfSafeMethod(settings.type)) {
                    xhr.setRequestHeader('X-CSRFToken', csrftoken)
                }
            }
        });
    }

    function __csrfSafeMethod(method) {
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    return {
        init: init(),
        setupAjaxForCSRF: setupAjaxForCSRF
    }

}();  // This class autoruns itself