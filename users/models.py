#encoding:utf-8

import datetime

from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _


def validate_date(value):
    if value >= datetime.date.today():
        raise ValidationError(_('This date must be prior than today.'))


class Actor(AbstractUser):
    """
    All the fields are required except the phone.
    """
    address = models.CharField(_('address'), max_length=150)
    phone = models.CharField(_('phone'), max_length=15,
                             validators=[RegexValidator('^([+-]\d+\s+)?(\([0-9]+\)\s+)?([\d\w\s-]+)$')], blank=True)
    date_of_birth = models.DateField(_('date of birth'), validators=[validate_date])

    GENDER_CHOICES = (
        ('M', _('Man')),
        ('W', _('Woman')),
    )
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)

    REQUIRED_FIELDS = ['first_name', 'last_name', 'gender', 'email', 'address', 'date_of_birth']

    def __str__(self):
        return self.first_name + ' ' + self.last_name


Actor._meta.get_field('first_name').blank = False
Actor._meta.get_field('last_name').blank = False
Actor._meta.get_field('email').blank = False
