/**
 * Login functions file
 *
 * Contains all the code used in the login screen
 *
 * Author: Lorenzo Ruiz
 * Version: 1.0
 *
 */
var Login = function() {

    if( ! (this instanceof Login) ) {
    	return new Login();
    }

    var languageSelector = null;

    function init() {
        languageSelector = $('#language');

        __attachEvents();
    }

    function __attachEvents() {
        languageSelector.change(__changeLanguage);
    }

    function __changeLanguage() {
        Utils.setupAjaxForCSRF();

        // $.post('/i18n/setlang/', { 'language': languageSelector.find(':selected').val() }).done(function(data) {
        //     alert(data);
        //     location.reload();
        // });
        $('html').load('/i18n/setlang/', { 'language': languageSelector.find(':selected').val() });
    }

    return {
        init: init()
    }

};

$(document).ready(function() {

    new Login();

});