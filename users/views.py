#encoding:utf-8

from django.contrib.auth import authenticate, login, logout
from django.core.urlresolvers import reverse
from django.http.response import HttpResponseRedirect
from django.shortcuts import render


def login_eteach(request):
    next_page = ""
    try:
        next_page = request.REQUEST['next']
    except KeyError:
        pass

    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('main'))

    if request.method == 'GET':
        return render(request, 'login-master.html', {'NEXT': next_page})

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)

        if next_page != "":
            return HttpResponseRedirect(next_page)

        return HttpResponseRedirect(reverse('login'))


def logout_eteach(request):
    logout(request)
    return HttpResponseRedirect(reverse('index'))