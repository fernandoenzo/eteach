#encoding:utf-8

from django.conf.urls import patterns, url


urlpatterns = patterns(
    '',
    url(r'^login/$', 'users.views.login_eteach', name='login'),
    url(r'^logout/$', 'users.views.logout_eteach', name='logout'),

)