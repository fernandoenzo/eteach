#encoding:utf-8

from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.utils.translation import ugettext_lazy as _
from django import forms


class ActorChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = get_user_model()


class ActorCreationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = get_user_model()

    def clean_username(self):
        username = self.cleaned_data["username"]
        try:
            get_user_model().objects.get(username=username)
        except get_user_model().DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'], code='duplicate_username')


class ActorAdmin(UserAdmin):
    add_form = ActorCreationForm
    form = ActorChangeForm

    fieldsets = (
        (_('Authentication'), {'classes': ('wide',), 'fields': ('username', 'password')}),
        (_('Personal info'), {'classes': ('wide',), 'fields':
            ('first_name', 'last_name', 'gender', 'email', 'phone', 'address', 'date_of_birth')}),
        (_('Permissions'), {'fields': ('is_active', 'is_superuser')}),
    )

    add_fieldsets = (
        (_('Authentication'), {'classes': ('wide',), 'fields': ('username', 'password1', 'password2')}),
        (_('Personal info'), {'classes': ('wide',), 'fields':
            ('first_name', 'last_name', 'gender', 'email', 'phone', 'address', 'date_of_birth')}),
    )


admin.site.register(get_user_model(), ActorAdmin)